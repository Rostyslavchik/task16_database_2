USE labor_sql;

#1.1
SELECT maker FROM product WHERE `type` IN ('pc');
#1.12
SELECT model, price FROM laptop
WHERE price > ALL (SELECT price FROM pc);
#1.7
#SELECT p.maker, p.model FROM product p
#WHERE p.type = 'pc' AND p.model = ALL (SELECT pc1.model FROM pc pc1 WHERE model = ALL (
#SELECT p2.model FROM product p2 WHERE p.maker = p2.maker AND p2.type = 'pc'));
#SELECT model FROM pc WHERE model = ALL (SELECT model FROM product WHERE type = 'pc')


#2.1
-
#2.4
SELECT p.maker FROM product p
WHERE EXISTS (SELECT model FROM pc WHERE p.model = model AND speed >= ALL (SELECT speed FROM pc))
AND EXISTS (SELECT maker FROM product WHERE type='printer');
#2.8
SELECT p.maker FROM product p
WHERE p.type='laptop' AND NOT EXISTS (SELECT maker FROM product WHERE p.maker = maker AND `type`='printer');


#3.1
SELECT CONCAT('середня ціна = ', AVG(price)) AS price_avg FROM laptop; 
#3.2
SELECT CONCAT('code:', code, ', model:', model, ', speed:', speed,
 ', ram:', ram, ', hd:', hd, ', cd:', cd, ', price:', price) AS info FROM pc;


#4.1
SELECT model, MAX(price) FROM printer;
#4.5
SELECT AVG(p.hd) FROM pc p, product prr WHERE prr.model = p.model 
AND EXISTS (SELECT pr1.model FROM product pr1 WHERE pr1.type = 'printer' AND pr1.maker = prr.maker)
GROUP BY prr.maker;

#5.1
SELECT p.maker, CONCAT('|pc:', (SELECT COUNT(*) FROM pc WHERE p.model = model),
 'laptop:', (SELECT COUNT(*) FROM laptop WHERE p.model = model),
 'printer:', (SELECT COUNT(*) FROM printer WHERE p.model = model)) AS info FROM product p;
#5.6


#6.1
SELECT p.maker,
	CASE type
		WHEN 'pc' THEN CONCAT('yes', (SELECT COUNT(*) FROM pc WHERE p.model = model))
		ELSE 'no'
	END AS pc
FROM product p;


#7.1

SELECT p.maker, p.model, p.type, pp1.price FROM product p, pc pp1 WHERE p.maker = 'B' AND p.model = pp1.model
UNION ALL
SELECT p.maker, p.model, p.type, lp1.price FROM product p, laptop lp1 WHERE p.maker = 'B' AND p.model = lp1.model
UNION ALL
SELECT p.maker, p.model, p.type, pr1.price FROM product p, printer pr1 WHERE p.maker = 'B' AND p.model = pr1.model;
#insert into PC values(12,'2111',900,128,40,'40x',980);