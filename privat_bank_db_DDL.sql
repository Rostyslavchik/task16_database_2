DROP DATABASE IF EXISTS privat_bank;
CREATE DATABASE privat_bank;

USE privat_bank;

CREATE TABLE `client` (
id INT NOT NULL AUTO_INCREMENT,
name VARCHAR(64) NOT NULL,
surname VARCHAR(64) NOT NULL,
city VARCHAR(64),
birthday DATE NOT NULL,
photo_link VARCHAR(64) NOT NULL,
PRIMARY KEY (id)
);

CREATE TABLE `account` (
id INT(16) UNSIGNED NOT NULL AUTO_INCREMENT,
dollar_amount DECIMAL(16,8) NOT NULL,
type ENUM('credit','simple','bonus'),
client_id INT NOT NULL,
PRIMARY KEY (id)
);

CREATE TABLE `another_bank_account` (
id INT(16) UNSIGNED NOT NULL AUTO_INCREMENT,
type ENUM('internet_bank','simple_bank','web-wallet'),
another_bank_tov VARCHAR(32) NULL,
PRIMARY KEY (id)
);

CREATE TABLE `another_bank` (
tov VARCHAR(32) NOT NULL,
another_bank_info_name VARCHAR(32) NOT NULL UNIQUE KEY,
PRIMARY KEY (tov)
);

CREATE TABLE `another_bank_info` (
name VARCHAR(32) NOT NULL,
founder VARCHAR(64),
PRIMARY KEY (name)
);

CREATE TABLE `operation` (
id INT NOT NULL AUTO_INCREMENT,
date DATE NOT NULL, 
type ENUM('send'),
dollar_amount DECIMAL(16,8) NOT NULL,
account_id INT(16) UNSIGNED NULL,
another_bank_account_id INT(16) UNSIGNED NULL,
PRIMARY KEY (id)
/*
CONSTRAINT CK_operation_account_id_another_bank_account_id CHECK (
	(account_id IS NULL OR another_bank_account_id IS NULL) 
	AND NOT (account_id IS NULL AND another_bank_account_id IS NULL))
    */ #it will be checked programmatically 
);

ALTER TABLE `account`
	ADD CONSTRAINT FK_account_client /* CONSTRAINT використовується для добавляння композитних ключів */
    FOREIGN KEY (client_id) REFERENCES `client`(id)
    ON DELETE CASCADE
    ON UPDATE CASCADE;

ALTER TABLE `operation`
	ADD CONSTRAINT FK_operation_account
    FOREIGN KEY (account_id) REFERENCES `account`(id)
    ON DELETE SET NULL
    ON UPDATE CASCADE;
    
ALTER TABLE `operation`
	ADD CONSTRAINT FK_operation_another_bank_account
    FOREIGN KEY (another_bank_account_id) REFERENCES `another_bank_account`(id)
    ON DELETE SET NULL
    ON UPDATE CASCADE;

ALTER TABLE `another_bank_account`
	ADD CONSTRAINT FK_another_bank_account_another_bank
    FOREIGN KEY (another_bank_tov) REFERENCES `another_bank`(tov)
    ON DELETE SET NULL
    ON UPDATE CASCADE;

ALTER TABLE `another_bank`
	ADD CONSTRAINT FK_another_bank_another_bank_info
    FOREIGN KEY (another_bank_info_name) REFERENCES `another_bank_info`(name)
    ON DELETE CASCADE
    ON UPDATE CASCADE;