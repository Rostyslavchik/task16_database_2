USE privat_bank;
INSERT INTO `client`(name, surname, city, birthday, photo_link) VALUES ('John', 'Smith', 'Oklahoma', '1998-05-23', 'http://John@gmail.com');
INSERT INTO `client`(name, surname, city, birthday, photo_link) VALUES ('Lilly', 'Nelson', 'New York', '1995-04-2', 'http://Lilly@gmail.com');
INSERT INTO `client`(name, surname, city, birthday, photo_link) VALUES ('Jimmy', 'Stalone', 'Lviv', '1991-11-3', 'http://Jimmy@gmail.com');
INSERT INTO `client`(name, surname, city, birthday, photo_link) VALUES ('Khristina', 'Deckson', 'Kyiv', '1992-7-22', 'http://Khristina@gmail.com');
INSERT INTO `client`(name, surname, city, birthday, photo_link) VALUES ('Andrew', 'Kruger', 'Minesota','1991-1-21', 'http://Andrew@gmail.com');

INSERT INTO `account`(dollar_amount, type, client_id) VALUES (3453.45, 'credit', 1);
INSERT INTO `account`(dollar_amount, type, client_id) VALUES (3452.35, 'simple', 2);
INSERT INTO `account`(dollar_amount, type, client_id) VALUES (967.45, 'credit', 2);
INSERT INTO `account`(dollar_amount, type, client_id) VALUES (6749, 'bonus', 2);
INSERT INTO `account`(dollar_amount, type, client_id) VALUES (7065.00, 'simple', 5);

INSERT INTO `another_bank_info`(name, founder) VALUES ('monobank', 'Mykola');
INSERT INTO `another_bank_info`(name, founder) VALUES ('qiwi', 'Sasha');

INSERT INTO `another_bank`(tov, another_bank_info_name) VALUES ('335w', 'monobank');
INSERT INTO `another_bank`(tov, another_bank_info_name) VALUES ('397j', 'qiwi');

INSERT INTO `another_bank_account`(type, another_bank_tov) VALUES ('simple_bank', '335w');
INSERT INTO `another_bank_account`(type, another_bank_tov) VALUES ('internet_bank', '335w');
INSERT INTO `another_bank_account`(type, another_bank_tov) VALUES ('web-wallet', '397j');

INSERT INTO `operation`(date, type, dollar_amount, account_id, another_bank_account_id) VALUES
('2021-05-23', 'send', 34.50, 1, NULL),
('2021-05-24', 'send', 445.76, NULL, 1),
('2021-05-2', 'send', 34, 2, NULL),
('2021-05-26', 'send', 0, 5, NULL),
('2021-05-27', 'send', 118.5045, NULL, 3);