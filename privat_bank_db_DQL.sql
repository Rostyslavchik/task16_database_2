USE privat_bank;

SELECT * FROM `client`;
SELECT * FROM `account`;
SELECT * FROM `another_bank_account`;
SELECT * FROM `operation`;
SELECT * FROM `another_bank`;
SELECT * FROM `another_bank_info`;

SELECT o.*, a.*, aa.* FROM `operation` o 
LEFT JOIN `account` a ON (o.account_id = a.id)
RIGHT JOIN `another_bank_account` aa ON (o.another_bank_account_id = aa.id); 
